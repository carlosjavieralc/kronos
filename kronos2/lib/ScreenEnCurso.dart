import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kronos2/Notas.dart';
import 'IntroSlide.dart';
import 'NotasEnCurso.dart';
import 'ScreenHome.dart';
import 'ScreenTips.dart';

class ScreenEnCurso extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            automaticallyImplyLeading: false,
            backgroundColor: Colors.yellow[600],
            actions: <Widget>[
              /*Boton cerrar sesión */
              IconButton(
                icon: Icon(Icons.home, color: Colors.black),
                onPressed: () {
                  /*Al presionar el este botón, se llama la función SignOutGoogle, y redireccionará al usuario 
              a la pagina donde todo inicia (PaginaIntro) */

                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ScreenHome()));
                },
              ),
            ],
            title: Text('Actividades En Curso',
                style: TextStyle(color: Colors.black))),
        body: StreamBuilder(
          /* Creamos esta instancia para ubicarnos en la base de datos que contiene los valores de la subasta en
         cuestión */

          /*La declaración .collection () toma el nombre de la colección que creamos en nuestra nube Firestore - 
      cloudfirestore. */

          stream: FirebaseFirestore.instance.collection('Notas2').snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) return CircularProgressIndicator();

            return ListView.builder(

                /*snapshot.data.documents.length es la longitud de los documentos que hemos creado en cloufirestore */
                itemCount: snapshot.data.docs.length,
                itemBuilder: (BuildContext context, int index) {
                  /*Subasas es un DocumentSnapshot, que sera el elemento que utilizaremos para acceder a los 
                    distintos campos. */

                  DocumentSnapshot data = snapshot.data.docs[index];

                  return NotasEnCurso(
                    documentSnapshot: data,
                    titulo: data['Titulo'],
                    descripcion: data['Descripcion'],
                  );
                });
          },
        ));
  }
}
