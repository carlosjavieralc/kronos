import 'dart:core';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

/*Datos de la Oferta: Clase principal.
Esta clase tendra los datos que se tienen en la base de datos en firebase */

class Notas extends StatefulWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
            title: Image.asset(
          'images/logo.png',
          height: 40,
        )),
        body: Notas(),
      ),
    );
  }

  final String id;
  final String titulo;
  final String descripcion;

  final DocumentSnapshot documentSnapshot;

  /*Constructor de la clase ProductItem */
  const Notas(
      {Key key, this.id, this.titulo, this.descripcion, this.documentSnapshot})
      : super(key: key);

  @override
  _NotasState createState() => _NotasState();
}

class _NotasState extends State<Notas> {
  Widget build(BuildContext context) {
    /*En una card expandible, se mostrará la infomación de la oferta */
    return ExpandableNotifier(
      child: Padding(
          padding: const EdgeInsets.all(6),
          child: Column(children: <Widget>[
            Slidable(
              actionPane: SlidableDrawerActionPane(),
              actionExtentRatio: 0.20,
              child: Padding(
                padding: const EdgeInsets.only(left: 0.0),
                child: Card(
                    color: Colors.orange[400],
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(15)),
                    clipBehavior: Clip.antiAlias,
                    child: Column(
                      children: <Widget>[
                        ScrollOnExpand(
                          scrollOnExpand: true,
                          scrollOnCollapse: false,
                          child: ExpandablePanel(
                            theme: const ExpandableThemeData(
                              headerAlignment:
                                  ExpandablePanelHeaderAlignment.center,
                              tapBodyToCollapse: true,
                            ),
                            header: Padding(
                              padding: EdgeInsets.all(10),
                              child: Text(
                                "${widget.titulo}",
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                            /*dentro de la card, la infomación principal que se mostrará es el sexo y el numero de la oferta  */
                            collapsed: Column(
                              textDirection: TextDirection.ltr,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 2.0),
                                  child: Text(
                                      "Descripción: ${widget.descripcion}",
                                      softWrap: true,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(color: Colors.black)),
                                ),
                                SizedBox(
                                  width: 96.0,
                                ),
                              ],
                            ),
                            /*Al momento que el usuario dese ver toda la información de la oferta, la card se expandira y mostrara la 
                              información complementaria */
                            expanded: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 5.0),
                                  child: Text("Descripción: ",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      )),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5.0),
                                  child: Text("${widget.descripcion}\n",
                                      style: TextStyle(color: Colors.black)),
                                ),
                                SizedBox(
                                  width: 96.0,
                                ),
                              ],
                            ),
                            builder: (_, collapsed, expanded) {
                              return Padding(
                                padding: EdgeInsets.only(
                                    left: 10, right: 10, bottom: 10),
                                child: Expandable(
                                  collapsed: collapsed,
                                  expanded: expanded,
                                  theme: const ExpandableThemeData(
                                      crossFadePoint: 0),
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    )),
              ),
              actions: <Widget>[
                Card(
                    child: Container(
                  color: Colors.blue,
                  child: IconButton(
                    icon: Image.asset(
                      'Iconos/libro.png',
                      color: Colors.white,
                      height: 20,
                    ),
                    onPressed: () {
                      FirebaseFirestore.instance.collection('Notas2').add({
                        'Titulo': '${widget.titulo}',
                        'Descripcion': '${widget.descripcion}'
                      });

                      FirebaseFirestore.instance
                          .collection('Notas')
                          .doc('${widget.documentSnapshot.id}')
                          .delete();
                    },
                  ),
                )),
                Card(
                    child: Container(
                  color: Colors.green,
                  child: IconButton(
                    icon: Image.asset(
                      'Iconos/libro.png',
                      color: Colors.white,
                      height: 20,
                    ),
                    onPressed: () {
                      FirebaseFirestore.instance.collection('Notas3').add({
                        'Titulo': '${widget.titulo}',
                        'Descripcion': '${widget.descripcion}'
                      });

                      FirebaseFirestore.instance
                          .collection('Notas')
                          .doc('${widget.documentSnapshot.id}')
                          .delete();
                    },
                  ),
                )),
              ],
              secondaryActions: <Widget>[
                Card(
                    child: Container(
                  color: Colors.red,
                  child: IconButton(
                    icon: Image.asset(
                      'Iconos/delete.png',
                      color: Colors.white,
                      height: 20,
                    ),
                    onPressed: () {
                      return showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Center(child: Text('Eliminar Nota')),
                              content: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      "¿Quiere eliminar esta nota?",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        FlatButton(
                                            child: Text('Si'),
                                            onPressed: () {
                                              FirebaseFirestore.instance
                                                  .collection("Notas")
                                                  .doc(
                                                      '${widget.documentSnapshot.id}')
                                                  .delete();
                                              Navigator.of(context).pop(true);
                                            }),
                                        FlatButton(
                                            child: Text('No'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            })
                                      ])
                                ],
                              ),
                            );
                          });
                    },
                  ),
                )),
              ],
            ),
            SizedBox(
              height: 6.0,
            ),
          ])),
    );
  }
}
