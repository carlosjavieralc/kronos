import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'ScreenInicioSesion.dart';

/*Función done inicia correr ela aplicación, en ella se sincroniza firebase mediante el 'async await' */
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(Inicio());
}

/* Clase para iniciar a recorrer la 'SplashScreen' donde se muestra el logo de Subasta en vivo y el
CircularProgressIndicator ( inidicador de progreso circular) , es decir, esta función llama la clase 
donde se encuntra la vista de la 'SplashScreen' */

class Inicio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Splash Screen',
      theme: ThemeData(primaryColor: Colors.white),
      home: ScreenSplash(),
      /*Llama a la clase ScreenSplash */
      debugShowCheckedModeBanner: false,
    );
  }
}

/* Vista de la SplashScreen */

class ScreenSplash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 4,
      navigateAfterSeconds: new ScreenInicioSesion(),
      image: Image.asset(
        'Images/Logo2.png',
      ),
      loadingText: Text("Cargando"),
      photoSize: 210.0,
      loaderColor: Colors.blue,
    );
  }
}
