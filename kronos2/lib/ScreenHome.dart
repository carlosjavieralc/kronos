import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:kronos2/FirebaseNotas.dart';
import 'package:kronos2/ScreenInicioSesion.dart';
import 'LoginGoogleService.dart';
import 'PaginasMenu.dart';
import 'ScreenTips.dart';

/* Clase donde inicia correr la primera vista cuando el usuario inicia sesión en la app , donde se
muestran todas las ofertas disponibles*/

class ScreenHome extends StatefulWidget {
  @override
  _ScreenHomeState createState() => _ScreenHomeState();
}

/*Esta clase contiene la información, que llevara la vista ProductItem */

class _ScreenHomeState extends State<ScreenHome> {
  int _selectedTabIndex = 0;

  List<Widget> tabPages = [ScreenTips(), FirebaseNotas()];

  _changeIndex(int index) {
    if (index != 1) {
      setState(() {
        _selectedTabIndex = index;
      });
    } else {
      Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => FirebaseNotas()),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.yellow[600],
          title: Text(
            'Inicio',
            style: TextStyle(color: Colors.black),
          ),
          leading: Builder(builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.menu,
                color: Colors.black,
              ),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          })),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Menú', style: TextStyle(color: Colors.black)),
              decoration: BoxDecoration(color: Colors.yellow[600]),
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text('Perfil', style: TextStyle(color: Colors.black)),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ScreenPerfil(),
                    ));
              },
            ),
            ListTile(
              leading: Icon(Icons.info_outline),
              title: Text('Acerca de Kronos',
                  style: TextStyle(color: Colors.black)),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ScreenInfoApp(),
                    ));
              },
            ),
            ListTile(
              leading: Icon(Icons.help_outline_sharp),
              title: Text('Ayuda', style: TextStyle(color: Colors.black)),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ScreenAyuda(),
                    ));
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('salir', style: TextStyle(color: Colors.black)),
              onTap: () {
                signOutGoogle();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ScreenInicioSesion()));
              },
            ),
          ],
        ),
      ),
      /*Necesitamos algo que controle el flujo de datos desde la base de datos a nuestra aplicación. para ello, 
      utilizamos un StreamBuilder*/

      /*El StreamBuilder toma el contexto, es decir que tipo de archivo vamos a utilizar y utiliza la instancia que
      utilizamos para ubicarnos en nuestro base de dats es cloudfirestore */

      body: Center(child: tabPages[_selectedTabIndex]),
      bottomNavigationBar: // sets the inactive color of the `BottomNavigationBar`
          BottomNavigationBar(
        backgroundColor: Colors.yellow[600],
        items: [
          BottomNavigationBarItem(
              icon: Image.asset(
                'Iconos/home.png',
                color: Colors.black,
                height: 20,
              ),
              title: new Text(
                "Inicio",
                style: new TextStyle(
                  color: const Color(0XFF000000),
                ),
              )),
          BottomNavigationBarItem(
            icon: Image.asset(
              'Iconos/notas.png',
              color: Colors.black,
              height: 20,
            ),
            title: new Text(
              "Actividades",
              style: new TextStyle(
                color: const Color(0XFF000000),
              ),
            ),
          ),
        ],
        onTap: _changeIndex,
        currentIndex: _selectedTabIndex,
      ),
    );
  }
}
