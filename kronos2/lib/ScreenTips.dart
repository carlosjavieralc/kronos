import 'package:flutter/material.dart';
import 'package:kronos2/IntroSlide.dart';

class ScreenTips extends StatelessWidget {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: new ListView(
        scrollDirection: Axis.horizontal,
        children: [
          Container(
              child: Container(
                  child: Image.asset(
            'Images/Tip1.jpeg',
            width: 400.0,
            height: 640.0,
            fit: BoxFit.cover,
          ))),
          Container(
              child: Container(
                  child: Image.asset(
            'Images/Tip2.jpeg',
            width: 400.0,
            height: 640.0,
            fit: BoxFit.cover,
          ))),
          Container(
            child: Container(
                child: Image.asset(
              'Images/Tip3.jpeg',
              width: 400.0,
              height: 640.0,
              fit: BoxFit.cover,
            )),
          ),
          Container(
            child: Container(
                child: Image.asset(
              'Images/Tip4.jpeg',
              width: 400.0,
              height: 640.0,
              fit: BoxFit.cover,
            )),
          ),
          Container(
              child: Container(
                  child: Image.asset(
            'Images/Tip5.jpeg',
            width: 400.0,
            height: 640.0,
            fit: BoxFit.cover,
          ))),
        ],
      ),
    ));
  }
}
