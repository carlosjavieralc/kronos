import 'package:kronos2/IntroSlide.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'LoginGoogleService.dart';

class ScreenInicioSesion extends StatefulWidget {
  @override
  _ScreenInicioSesionState createState() => _ScreenInicioSesionState();
}

class _ScreenInicioSesionState extends State<ScreenInicioSesion> {
  /* Creamos una instancia de FirebaseAuth. Esta instancia, puede manejar la creación de nuevos usuarios con correo 
 electrónico y contraseña. Del mismo modo,una vez que el usuario se registra correctamente, se redirige a la pantalla 
 de inicio de sesión.*/

  final _auth = FirebaseAuth.instance;

  bool showProgress =
      false; /* Variable definida para el proceso de ModalProggressHUD */

  String email,
      password; /* Variables para la recolección de datos para el inicio de sesión y registro */

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        /* ModalProgressHUD
        : Es una  dependencia de flutter que se utiliza para mostrará el indicador de progreso de 
        espera de registro o inicio de sesión cuando se haga clic en el botón ( registar o iniciar sesión)  hasta el 
        proceso de autenticación. */

        child: ModalProgressHUD(
          inAsyncCall: showProgress,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'Images/Logo.png',
                height: 150,
              ),
              SizedBox(
                height: 30.0,
              ),
              Text(
                "Inicio de sesión",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),
              ),
              SizedBox(
                height: 20.0,
              ),
              SizedBox(
                height: 20.0,
              ),
              SizedBox(height: 50),
              _signInButton(),
            ],
          ),
        ),
      ),
    );
  }

/*Botin de Inicoio de sesión */
  Widget _signInButton() {
    return OutlineButton(
      splashColor: Colors.grey,
      onPressed: () {
        signInWithGoogle().then((result) {
          if (result != null) {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) {
                  /*Si el inico de sesión es correcto, se redirecciona al usuario a la vista donde
                  se encuentran todas las ofertas disponibles */
                  return IntroSlide();
                },
              ),
            );
          }
        });
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      highlightElevation: 0,
      borderSide: BorderSide(color: Colors.grey),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'Iconos/logo_google.png',
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                'Iniciar sesión con Google ',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
