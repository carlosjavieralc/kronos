import 'package:flutter/material.dart';
import 'package:kronos2/ScreenHome.dart';
import 'package:nice_intro/intro_screen.dart';
import 'package:nice_intro/intro_screens.dart';
import 'package:tinycolor/tinycolor.dart';

class IntroSlide extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'intro screen demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: IntroApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class IntroApp extends StatefulWidget {
  @override
  IntroAppState createState() {
    return IntroAppState();
  }
}

class IntroAppState extends State<IntroApp> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    var screens = IntroScreens(
      onDone: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ScreenHome(),
        ),
      ),
      onSkip: () => print('Desliza'),
      footerBgColor: TinyColor(Colors.yellow[600]).color,
      footerRadius: 18.0,
      textColor: Colors.black,
      indicatorType: IndicatorType.CIRCLE,
      slides: [
        IntroScreen(
          title: 'Planifica tus Actividades',
          imageAsset: 'Images/Slide1.jpg',
          description: 'Kronos Te ayuda a planificar tus Actividades',
          headerBgColor: Colors.white,
        ),
        IntroScreen(
          title: 'Administra tu Tiempo',
          headerBgColor: Colors.white,
          imageAsset: 'Images/Slide2.jpg',
          description: "Kronos te ayuda a que administres tu tiempo",
        ),
        IntroScreen(
          title: 'Kronos',
          headerBgColor: Colors.white,
          imageAsset: 'Images/Slide3.jpg',
          description: "¡Usa Kronos, Te ayuda a organizarte!",
        ),
      ],
    );

    return Scaffold(
      body: screens,
    );
  }
}
