import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ScreenPerfil extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.yellow[600],
          centerTitle: true,
          title: Text('Perfil',
              style:
                  TextStyle(fontStyle: FontStyle.normal, color: Colors.black)),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: Column(children: [
          SizedBox(
            height: 30,
          ),
          Image.asset('Images/Logo.png'),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.all(6),
            child: Container(
              child: Padding(
                  padding: const EdgeInsets.all(6),
                  child: Text(
                    'Bienvenido(a) a Kronos\n',
                    style: TextStyle(
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.bold,
                        fontSize: 30),
                  )),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(6),
            child: Container(
              child: Padding(
                  padding: const EdgeInsets.all(6),
                  child: Text(
                      'Kronos te permitirá gestionar tu tiempo y agendar tus actividades para que puedas cumplor con cada una de ellas. kronos etsaba basada en el tablero kanvas la cual es una metodologia para el desarrollo de proyectos',
                      textAlign: TextAlign.justify)),
            ),
          ),
        ]));
  }
}

class ScreenInfoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.yellow[600],
          centerTitle: true,
          title: Text('Acerca de Kronos',
              style:
                  TextStyle(fontStyle: FontStyle.normal, color: Colors.black)),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: Column(children: [
          SizedBox(
            height: 30,
          ),
          Image.asset('Images/Logo.png'),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.all(6),
            child: Container(
              child: Padding(
                  padding: const EdgeInsets.all(6),
                  child: Text(
                      'Kronos es la adaptación del tablero kanban, para permitirle a los estudiantes de la Universidad de Cartagena gestionar adecuadamente su tiempo y asi logren cumplir con las actividades corresponcientes a su proceso académico y profesional clasificando las actividades en : pendientes, en curso y realizadas',
                      textAlign: TextAlign.justify)),
            ),
          ),
        ]));
  }
}

class ScreenAyuda extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.yellow[600],
          centerTitle: true,
          title: Text('Ayuda',
              style:
                  TextStyle(fontStyle: FontStyle.normal, color: Colors.black)),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: ListView(children: [
          Container(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 0.0),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Row(children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 25.0),
                          child: Text(
                            ' ¿ Como agregar una actividad ?',
                            style: TextStyle(
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        )
                      ])
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Image.asset('Images/ayuda1.jpg')
              ],
            ),
          ),
          Container(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Row(children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 25.0),
                          child: Text(
                            ' ¿Como mover  una actividad?',
                            style: TextStyle(
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        )
                      ])
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Image.asset('Images/ayuda2.jpg')
              ],
            ),
          ),
          Container(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Row(children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 25.0),
                          child: Text(
                            ' ¿ Como eliminar una actividad ?',
                            style: TextStyle(
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        )
                      ])
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Image.asset('Images/ayuda3.jpg')
              ],
            ),
          ),
        ]));
  }
}
