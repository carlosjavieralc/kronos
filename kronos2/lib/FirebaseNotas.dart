import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:kronos2/Notas.dart';
import 'package:kronos2/ScreenPendientes.dart';

import 'ScreenEnCurso.dart';
import 'ScreenTerminadas.dart';

/* Clase donde inicia correr la primera vista cuando el usuario inicia sesión en la app , donde se
muestran todas las ofertas disponibles*/

class FirebaseNotas extends StatefulWidget {
  @override
  _FirebaseNotasState createState() => _FirebaseNotasState();
}

/*Esta clase contiene la información, que llevara la vista ProductItem */

class _FirebaseNotasState extends State<FirebaseNotas> {
  var nota = Notas();
  int _selectedTabIndex = 0;
  String titulo, descripcion;

  List<Widget> tabPages = [
    ScreenPendientes(),
    ScreenEnCurso(),
    ScreenTerminadas(),
  ];
  _changeIndex(int index) {
    setState(() {
      _selectedTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    _showDialog(BuildContext ctx) {
      showDialog(
          context: ctx,
          builder: (context) {
            return AlertDialog(
                title: Center(child: Text("Agregar Nota")),
                content: Container(
                    height: 250,
                    width: 150,
                    child: Column(children: [
                      Padding(
                          padding:
                              const EdgeInsets.only(left: 20.0, right: 00.0),
                          child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            textAlign: TextAlign.center,
                            onChanged: (value) {
                              titulo =
                                  value; // Se obtiene el valor ingresado: correo electronico
                            },
                            decoration: InputDecoration(
                              hintText: "Ingrese Titulo",
                            ),
                          )),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                          padding:
                              const EdgeInsets.only(left: 20.0, right: 20.0),
                          child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            textAlign: TextAlign.center,
                            onChanged: (value) {
                              descripcion =
                                  value; // Se obtiene el valor ingresado: correo electronico
                            },
                            decoration: InputDecoration(
                              hintText: "Ingrese Descripción ",
                            ),
                          )),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                          padding: const EdgeInsets.only(left: 50.0),
                          child: Column(children: [
                            Row(children: [
                              IconButton(
                                  icon: Image.asset(
                                    'Iconos/libro.png',
                                    color: Colors.red,
                                    height: 20,
                                  ),
                                  onPressed: () {}),
                              IconButton(
                                  icon: Image.asset(
                                    'Iconos/libro.png',
                                    color: Colors.blue,
                                    height: 20,
                                  ),
                                  onPressed: () {}),
                              IconButton(
                                  icon: Image.asset(
                                    'Iconos/libro.png',
                                    color: Colors.green,
                                    height: 20,
                                  ),
                                  onPressed: () {}),
                            ]),
                          ])),
                      Padding(
                          padding:
                              const EdgeInsets.only(left: 15.0, right: 20.0),
                          child: Row(
                            children: [
                              OutlineButton(
                                onPressed: () {},
                                child: Text("Cancelar"),
                                borderSide: BorderSide(color: Colors.red),
                                shape: StadiumBorder(),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                    side: BorderSide(color: Colors.green)),
                                onPressed: () {
                                  FirebaseFirestore.instance
                                      .collection('Notas')
                                      .add({
                                    'Titulo': titulo,
                                    'Descripcion': descripcion,
                                  });
                                },
                                color: Colors.green,
                                textColor: Colors.white,
                                child: Text("Agregar",
                                    style: TextStyle(fontSize: 14)),
                              ),
                            ],
                          ))
                    ])));
          });
    }

    return Scaffold(
      /*Necesitamos algo que controle el flujo de datos desde la base de datos a nuestra aplicación. para ello, 
      utilizamos un StreamBuilder*/

      /*El StreamBuilder toma el contexto, es decir que tipo de archivo vamos a utilizar y utiliza la instancia que
      utilizamos para ubicarnos en nuestro base de dats es cloudfirestore */

      body: Center(child: tabPages[_selectedTabIndex]),
      bottomNavigationBar: // sets the inactive color of the `BottomNavigationBar`
          BottomNavigationBar(
        backgroundColor: Colors.yellow[600],
        items: [
          BottomNavigationBarItem(
              icon: Image.asset(
                'Iconos/libro.png',
                color: Colors.red,
                height: 30,
              ),
              title: new Text(
                "Pendientes",
                style: new TextStyle(
                  color: const Color(0XFF000000),
                ),
              )),
          BottomNavigationBarItem(
              icon: Image.asset(
                'Iconos/libro.png',
                color: Colors.blue,
                height: 30,
              ),
              title: new Text(
                "En curso",
                style: new TextStyle(
                  color: const Color(0XFF000000),
                ),
              )),
          BottomNavigationBarItem(
              icon: Image.asset(
                'Iconos/libro.png',
                color: Colors.green,
                height: 30,
              ),
              title: new Text(
                "Terminadas",
                style: new TextStyle(
                  color: const Color(0XFF000000),
                ),
              )),
        ],
        onTap: _changeIndex,
        currentIndex: _selectedTabIndex,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showDialog(context);
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.black,
      ),
    );
  }
}
